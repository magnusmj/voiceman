import numpy as np
from PyQt5.QtCore import QThread, pyqtSignal


class Tone:
	def __init__(self, chunk_size, harmonics):
		self._frequency_offset = 1.0
		self.frequency = 0.0
		self.amplitudes = np.zeros(harmonics)
		self.phases = [0] * harmonics
		self._chunk_size = chunk_size
		self._sample_rate = 44100
		self._freq_change = np.fromfunction(lambda x, y: y ** 2, (1, chunk_size), dtype=int)[0] / (chunk_size ** 2)
		#self._freq_lerp = np.fromfunction(lambda x, y: 1/(y+1), (1, harmonics), dtype=int)[0]
		self._freq_lerp = np.linspace(1, 0.2, harmonics)
		self._harmonic_profiles = np.ones(harmonics)
		self._harmonic_factors = np.zeros(harmonics)


	def create_tone(self, frequency, amplitudes):
		tone = np.zeros(self._chunk_size)

		old_amps = np.copy(self.amplitudes)
		old_frq = np.copy(self.frequency)
		max_val = amplitudes[0]
		amplitudes *= self._harmonic_profiles
		amplitudes += self._harmonic_factors*max_val
		delta_amps = amplitudes-old_amps
		delta_amps_pos = np.clip(delta_amps, a_min=0, a_max=None)
		delta_amps_neg = np.clip(delta_amps, a_min=None, a_max=0)
		self.amplitudes += np.multiply(self._freq_lerp, delta_amps_pos)
		self.amplitudes += delta_amps_neg
		if frequency < 50:
			frequency = old_frq
		else:
			frequency *= self._frequency_offset
		self.frequency += (frequency-old_frq)*0.1

		for i in range(len(amplitudes)):
			amplitude = self.amplitudes[i]
			old_amp = old_amps[i]
			old_revs = 2 * old_frq * (1 + i) * self._chunk_size / self._sample_rate
			revs = 2 * self.frequency * (1 + i) * self._chunk_size / self._sample_rate

			phase_array = np.linspace(self.phases[i], self.phases[i] + np.pi * old_revs, self._chunk_size)
			phase_array += self._freq_change*(revs-old_revs)

			if i % 2 == 0:
				tone -= np.multiply(np.sin(phase_array), np.linspace(old_amp, amplitude, self._chunk_size))
			else:
				tone += np.multiply(np.sin(phase_array), np.linspace(old_amp, amplitude, self._chunk_size))
			self.phases[i] = phase_array[self._chunk_size-1] + (np.pi * revs / self._chunk_size)
			self.phases[i] %= 2 * np.pi
		return tone

	@property
	def frequency_offset(self):
		return self._frequency_offset

	@frequency_offset.setter
	def frequency_offset(self, value):
		self._frequency_offset = float(value)

	@property
	def harmonic_profiles(self):
		return self._harmonic_profiles

	@property
	def harmonic_factors(self):
		return self._harmonic_factors

class VoiceManThread(QThread):
	finished_signal = pyqtSignal('PyQt_PyObject')
	new_chunk_signal = pyqtSignal('PyQt_PyObject')
	new_freq_signal = pyqtSignal('PyQt_PyObject')

	def __init__(self, chunk_size=1024, main_chunk_size=4096, tone_harmonics=16):
		QThread.__init__(self)
		self._alive = True
		self._chunks = []
		self._main_chunk_size = main_chunk_size
		self._main_chunk = np.zeros(main_chunk_size, dtype=np.int16).tobytes()
		self._chunk_size = chunk_size
		self._tone_harmonics = tone_harmonics
		self._main_tone = Tone(chunk_size, self._tone_harmonics)
		self._sample_rate = 44100
		self._last_highpass_trim = None
		self._last_i = 0
		self._last_prci = 0

	@property
	def frequency_offset(self):
		return self._main_tone.frequency_offset

	@frequency_offset.setter
	def frequency_offset(self, value):
		self._main_tone.frequency_offset = float(value)

	def on_pitch_changed(self, value):
		self.frequency_offset = value

	def on_new_chunk(self, chunk):

		self._main_chunk = self._main_chunk+chunk
		self._main_chunk = self._main_chunk[len(chunk):]
		self._chunks.append(chunk)

	def on_stop(self):
		self._alive = False

	def get_magnitudes(self, fft_data):
		fs = self._sample_rate
		freq_bins = np.fft.fftfreq(self._main_chunk_size) * fs
		arlen = self._main_chunk_size // 2

		normalization_data = fft_data / self._main_chunk_size
		magnitude_values = np.abs(normalization_data[:arlen]) + np.abs(np.insert(np.flip(normalization_data[arlen:]), 0, 0)[:arlen])

		log_length = 150
		log_freq_bins = np.logspace(0, 9, num=log_length, base=3) + np.linspace(0, 3500, log_length)
		log_magnitudes = np.zeros(log_length)
		i = 0
		z = freq_bins[i]
		j = 0
		for j in range(len(log_freq_bins)):
			x = log_freq_bins[j]
			while z < x and i < arlen:
				log_magnitudes[j] += magnitude_values[i]
				z = freq_bins[i]
				i += 1
		return [freq_bins, magnitude_values, log_freq_bins, log_magnitudes]

	def apply_high_pass(self, fft_data):
		filter_size = 128 * self._main_chunk_size // 2048
		trim_size = self._chunk_size//8
		arlen = self._chunk_size // 2
		fft_data[:filter_size+1] = 0
		fft_data[2 * self._main_chunk_size//2 - filter_size:] = 0
		in_trim = np.linspace(0, 1, trim_size)
		out_trim = np.linspace(1, 0, trim_size)
		chunk = np.fft.ifft(fft_data, self._main_chunk_size).astype(np.float)
		chunk = chunk[self._main_chunk_size//2-self._chunk_size//2:self._main_chunk_size//2+self._chunk_size//2]
		if self._last_highpass_trim is not None:
			chunk[:trim_size] *= in_trim
			chunk[:trim_size] += self._last_highpass_trim[trim_size:]*out_trim
		self._last_highpass_trim = np.copy(chunk[arlen-trim_size:arlen+trim_size])
		chunk[self._chunk_size-trim_size:] *= out_trim
		chunk[self._chunk_size-trim_size:] += self._last_highpass_trim[:trim_size]*in_trim
		return chunk

	def find_voice_attributes(self, freq_bins, magnitude_values, chunk):

		fft_data = np.abs(np.fft.fft(np.frombuffer(chunk, dtype=np.int16)))
		i = 48*self._main_chunk_size//self._chunk_size
		a_max = 2*(max(np.amax(fft_data[:i]), np.amax(fft_data[len(fft_data)-i:])))/self._chunk_size

		i = 0
		index = 3
		peak_detected = False

		last_amp = 0
		while index < 32:
			if peak_detected:
				if last_amp > magnitude_values[index]:
					i = index-1
					break
				else:
					last_amp = magnitude_values[index]
			if magnitude_values[index] > 150 and not peak_detected and magnitude_values[index] > magnitude_values[index-1]:
				if magnitude_values[index] / (magnitude_values[index-1]+0.1) > 2:
					peak_detected = True
					last_amp = magnitude_values[index]
			index += 1

		if self._last_i != 0:
			while i / self._last_i > 1.9:
				i = i//2
			a = magnitude_values[i]
			self._last_i = i

		prci = 1
		frq = freq_bins[i]
		a = magnitude_values[i]
		if i > 1:
			al = magnitude_values[i - 1]
			ah = magnitude_values[i + 1]
			frl = freq_bins[i - 1]
			frh = freq_bins[i + 1]
			frq_ratio = (a - ah) / (a - al + a - ah)
			frq = frh * (1 - frq_ratio) + frl * frq_ratio
			if frq_ratio < 0 or frq_ratio > 1:
				frq_ratio = np.clip(frq_ratio, 0, 1)
			prci = (i+1) * (1 - frq_ratio) + (i-1) * frq_ratio

			if self._last_prci != 0:
				while prci / self._last_prci > 1.9:
					prci = prci / 2
			self._last_prci = prci

		amps = np.zeros(self._tone_harmonics)
		largest_amp = 0
		for index in range(1, self._tone_harmonics+1):
			i = int(round(index*prci))
			if i > 1 and i < self._chunk_size/2-2:

				back = min(magnitude_values[i+2], magnitude_values[i+1])
				back += min(magnitude_values[i-2], magnitude_values[i-1])
				back /= 4
				if back < magnitude_values[i]:
					#amp = 2*(magnitude_values[i]-back)
					amp = magnitude_values[i]
					largest_amp = max(largest_amp, amp)
					amps[index-1] = amp

		if largest_amp < 150 or prci == 1:
			amps = np.zeros(self._tone_harmonics)
		else:
			pass
			#amps /= largest_amp
			#amps *= a_max

		#print(str(prci) + str(amps))
		return frq, amps

	def add_synth_voice(self, new_chunk, voice_attr):
		pass
		new_chunk += self._main_tone.create_tone(voice_attr[0], voice_attr[1])

	def harmonic_profile_changed(self, change):
		self._main_tone.harmonic_profiles[change['index']] = change['value']

	def harmonic_factor_changed(self, change):
		self._main_tone.harmonic_factors[change['index']] = change['value']

	def run(self):
		self._alive = True
		arlen = self._chunk_size // 2
		while self._alive:
			if len(self._chunks) > 0:
				chunk = self._chunks.pop(0)
				#ints = np.frombuffer(chunk, dtype=np.int16)
				ints = np.frombuffer(self._main_chunk, dtype=np.int16)
				fft_data = np.fft.fft(ints)
				[freq_bins, magnitude_values, log_freq_bins, log_magnitudes] = self.get_magnitudes(fft_data)
				self.new_freq_signal.emit(log_magnitudes.tobytes())
				new_chunk = self.apply_high_pass(fft_data)
				voice_attr = self.find_voice_attributes(freq_bins, magnitude_values, chunk)
				self.add_synth_voice(new_chunk, voice_attr)
				self.new_chunk_signal.emit(new_chunk.astype(np.int16).tobytes())

			else:
				self.msleep(10)
		self.finished_signal.emit(0)