from PyQt5.QtCore import QThread, pyqtSignal
import pyaudio


class VoiceInThread(QThread):
	finished_signal = pyqtSignal('PyQt_PyObject')
	new_chunk_signal = pyqtSignal('PyQt_PyObject')

	def __init__(self, audio, chunk_size=1024):
		QThread.__init__(self)
		self._audio = audio
		self._alive = True
		self._chunks = []
		self._chunk_size = chunk_size

	def on_stop(self):
		self._alive = False

	def run(self):
		self._alive = True

		p = self._audio  # pyaudio.PyAudio()
		sample_format = pyaudio.paInt16
		channels = 1
		fs = 44100

		instream = p.open(format=sample_format, channels=channels, rate=fs, frames_per_buffer=self._chunk_size, input=True)

		while self._alive:
			chunk = instream.read(self._chunk_size)
			self.new_chunk_signal.emit(chunk)

		instream.close()

		self.finished_signal.emit(0)