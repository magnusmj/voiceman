import os
from datetime import datetime

from PyQt5.QtCore import QThread, pyqtSignal
import pyaudio
import wave


class VoiceOutThread(QThread):
	finished_signal = pyqtSignal('PyQt_PyObject')

	def __init__(self, audio, chunk_size=1024):
		QThread.__init__(self)
		self._audio = audio
		self._alive = False
		self._chunks = []
		self._chunk_size = chunk_size
		self._lastChunk = bytes(chunk_size*4)
		self._frame_rate = 44100
		self._recording = False
		self._wf = None

	def on_new_chunk(self, chunk):
		self._chunks.append(chunk)

	def on_stop(self):
		self._alive = False

	@property
	def recording(self):
		return self._recording

	@recording.setter
	def recording(self, value):
		if self._alive and value:
			self.start_record()
		if self._alive and not value:
			self.stop_record()
		self._recording = value

	def start_record(self):
		folder = os.path.expanduser('~/')
		filename = datetime.now().strftime("%d-%m-%Y-%H:%M:%S") + ".wav"
		self._wf = wave.open(folder+filename, 'wb')
		self._wf.setnchannels(1)
		self._wf.setsampwidth(self._audio.get_sample_size(pyaudio.paInt16))
		self._wf.setframerate(self._frame_rate)

	def stop_record(self):
		self._wf.close()

	def run(self):
		self._alive = True
		# Set chunk size of 1024 samples per data frame
		p = self._audio
		sample_format = pyaudio.paInt16
		channels = 1

		if self._recording:
			self.start_record()

		out_stream = p.open(format=sample_format, channels=channels, rate=self._frame_rate, output=True)

		while self._alive:
			if len(self._chunks) > 0:
				chunk = self._chunks.pop(0)
				out_stream.write(chunk, self._chunk_size)
				self._lastChunk = chunk
				if self._recording:
					self._wf.writeframes(chunk)
			else:
				out_stream.write(self._lastChunk, self._chunk_size)

		out_stream.close()
		if self._recording:
			self.stop_record()

		self.finished_signal.emit(0)