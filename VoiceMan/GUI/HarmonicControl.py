from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QSlider, QLabel


class HarmonicControl(QWidget):
	def __init__(self, parent, index):
		QWidget.__init__(self, parent)
		self._index = index
		self._main_window = parent
		self.setLayout(QVBoxLayout())

		amp_profile_slider = QSlider(Qt.Vertical)
		amp_profile_slider.setMaximum(400)
		amp_profile_slider.setValue(100)
		amp_profile_slider.valueChanged.connect(self.on_amp_profile_changed)
		self.layout().addWidget(amp_profile_slider)

		amp_factor_slider = QSlider(Qt.Vertical)
		amp_factor_slider.setMaximum(200)
		amp_factor_slider.setValue(0)
		amp_factor_slider.valueChanged.connect(self.on_amp_factor_changed)
		self.layout().addWidget(amp_factor_slider)

		self.layout().addWidget(QLabel("H\n" + str(index)))

	def on_amp_profile_changed(self, e):
		change = {
			'index': self._index,
			'value': e / 100
		}
		self._main_window.voice_man.harmonic_profile_changed(change)

	def on_amp_factor_changed(self, e):
		change = {
			'index': self._index,
			'value': e / ((1+self._index)*100)
		}
		self._main_window.voice_man.harmonic_factor_changed(change)