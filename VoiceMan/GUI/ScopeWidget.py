import numpy as np
from PyQt5.QtCore import Qt, QPointF, QLine, QLineF, QTimer

from PyQt5.QtGui import QPaintEvent, QPainter, QBrush, QPainterPath, QPolygonF
from PyQt5.QtWidgets import QWidget


class ScopeWidget(QWidget):
	def __init__(self, parent, chunk_type=np.int16, max_magnitude=65536, divisor=1, offset=0.5, concatenate=-1):
		QWidget.__init__(self, parent)
		self._chunks = []
		self._concatenate_buffer = []
		self._timer = QTimer()
		self._timer.timeout.connect(self.tick)
		self._timer.start(1000 / 60)
		self._chunk_type = chunk_type
		self._y = 0
		self._divisor = divisor
		self._max_magnitude = max_magnitude
		self._offset = offset
		self._concatenate = concatenate
		self.setMaximumHeight(200)
		self.setMinimumHeight(200)

	def tick(self):
		if len(self._chunks) == 0:
			return
		self.update()

	def on_new_chunk(self, chunk):
		if self._concatenate != -1:
			self._concatenate_buffer.append(np.frombuffer(chunk, dtype=self._chunk_type))
			if len(self._concatenate_buffer) > self._concatenate:
				a1 = self._concatenate_buffer.pop(0)
				a2 = self._concatenate_buffer.pop(0)
				new_chunk = np.concatenate((a1, a2))
				if self._concatenate > 2:
					for i in range(2, self._concatenate):
						new_chunk = np.concatenate((new_chunk, self._concatenate_buffer.pop(0)))
				self._chunks.append(new_chunk)
		else:
			self._chunks.append(np.frombuffer(chunk, dtype=self._chunk_type))
		#self.update()

	def paintEvent(self, event: QPaintEvent):
		chunks = self._chunks
		if len(chunks) == 0:
			return
		qp = QPainter()
		qp.begin(self)
		rect = event.rect()
		qp.setBrush(QBrush(Qt.black))
		qp.drawRect(rect)
		qp.setBrush(QBrush(Qt.NoBrush))
		qp.setPen(Qt.green)
		chunk = chunks.pop(0).astype(float)
		if len(chunks)>1:
			chunks.clear()
		count = len(chunk)

		w_ratio = rect.width()/count
		i = 0

		chunk = rect.height() * chunk / self._max_magnitude

		divisor = self._divisor
		path = QPainterPath()
		path.moveTo(0, rect.height() * self._offset)
		j = 0
		for frame in chunk:
			if j == 0:
				self._y = 0
			self._y += frame
			if j < divisor-1:
				j += 1
			else:
				x = i * w_ratio
				path.lineTo(x, rect.height() * self._offset - self._y/divisor)
				j = 0

			i += 1

		qp.drawPath(path)
		qp.end()
