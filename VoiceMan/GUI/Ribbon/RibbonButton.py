# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtCore import QSize, QRectF, Qt
from PyQt5.QtGui import QPainter, QFontMetrics, QFont
from PyQt5.QtWidgets import *

from VoiceMan.GUI import gui_scale, get_stylesheet

__author__ = 'magnus'


class RibbonButton(QToolButton):
	def __init__(self, owner, action, is_large):
		QPushButton.__init__(self, owner)
		# sc = 1
		sc = gui_scale()
		self._sc = sc
		if is_large:
			self.text = action.text()
		else:
			self.text = action.text().replace('\n', ' ')
		self._action = action
		self.textWidth = 50 * sc
		self.clicked.connect(self._action.trigger)
		self._action.changed.connect(self.update_button_status_from_action)
		self.font = QFont()
		#self.font.setPointSizeF(10*sc)
		fm = QFontMetrics(QFont())
		for t in self.text.split("\n"):
			self.textWidth = max(self.textWidth, fm.width(t)+10)

		if is_large:
			self.setStyleSheet(get_stylesheet("ribbonButton"))
			self.setToolButtonStyle(3)
			self.setIconSize(QSize(32 * sc, 32 * sc))
			self.setMaximumWidth(90 * sc)
			self.setMinimumWidth(50 * sc)
			self.setMinimumHeight(80 * sc)
			self.setMaximumHeight(95 * sc)
		else:
			self.setToolButtonStyle(2)
			self.setMinimumWidth(24*sc + self.textWidth)
			self.setMaximumWidth(24*sc + self.textWidth)
			self.setMaximumHeight(24 * sc)
			self.setIconSize(QSize(16 * sc, 16 * sc))
			self.setStyleSheet(get_stylesheet("ribbonSmallButton"))

		self.update_button_status_from_action()

	def paintEvent(self, event):
		sc = self._sc
		if self.toolButtonStyle() == 3:
			qp = QPainter()
			qp.begin(self)
			qp.setFont(self.font)
			qp.drawText(QRectF(0, 32*sc, self.textWidth, 60*sc), Qt.AlignHCenter | Qt.AlignVCenter, self.text)
			qp.end()

		return super(RibbonButton, self).paintEvent(event)

	def sizeHint(self):
		return QSize(self.textWidth, 85*gui_scale())

	def update_button_status_from_action(self):
		if self.toolButtonStyle() == 3:
			pass
		else:
			self.setText(self._action.text().replace('\n', ' '))
		self.setStatusTip(self._action.statusTip())
		self.setToolTip(self._action.toolTip())
		self.setIcon(self._action.icon())
		self.setEnabled(self._action.isEnabled())
		self.setCheckable(self._action.isCheckable())
		self.setChecked(self._action.isChecked())
