# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt

from VoiceMan.GUI import tr
from VoiceMan.GUI.Ribbon.RibbonPane import RibbonPane


class RibbonTab(QWidget):
	def __init__(self, parent, name):
		QWidget.__init__(self, parent)
		layout = QHBoxLayout()
		self.setLayout(layout)
		layout.setContentsMargins(0, 0, 0, 0)
		layout.setSpacing(0)
		layout.setAlignment(Qt.AlignLeft)
		self._panes = {}

	def add_ribbon_pane(self, name):
		name = tr(name, "ribbon")
		ribbon_pane = RibbonPane(self, name)
		self._panes[name] = ribbon_pane
		self.layout().addWidget(ribbon_pane)
		return ribbon_pane

	def get_ribbon_pane(self, name):
		name = tr(name, "ribbon")
		if name in self._panes:
			return self._panes[name]
		return self.add_ribbon_pane(name)

	def add_spacer(self):
		self.layout().addSpacerItem(QSpacerItem(1, 1, QSizePolicy.MinimumExpanding))
		self.layout().setStretch(self.layout().count() - 1, 1)
