# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from PyQt5.QtWidgets import *
from VoiceMan.GUI.Ribbon.RibbonTab import RibbonTab
from VoiceMan.GUI import gui_scale, tr

__author__ = 'magnus'


class RibbonWidget(QTabWidget):
	def __init__(self, main_window):
		QWidget.__init__(self, main_window)

		self.setMaximumHeight(150 * gui_scale())
		self.setMinimumHeight(100 * gui_scale())
		self._tabs = {}

	def add_ribbon_tab(self, name):
		ribbon_tab = RibbonTab(self, tr(name, "ribbon"))
		self.addTab(ribbon_tab, name)
		self._tabs[name] = ribbon_tab
		return ribbon_tab

	def get_ribbon_tab(self, name):
		if name in self._tabs:
			return self._tabs[name]
		return self.add_ribbon_tab(name)
