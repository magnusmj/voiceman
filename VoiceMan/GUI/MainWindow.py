import numpy as np
import pyaudio
from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QKeySequence
from PyQt5.QtWidgets import QMainWindow, QToolBar, QAction, QWidget, QVBoxLayout, QHBoxLayout, QSlider

from VoiceMan.GUI import is_dark_theme, gui_scale, get_icon, tr, get_stylesheet
from VoiceMan.GUI.HarmonicControl import HarmonicControl
from VoiceMan.GUI.Ribbon.RibbonButton import RibbonButton
from VoiceMan.GUI.ScopeWidget import ScopeWidget
from VoiceMan.LOGIC.VoiceInThread import VoiceInThread
from VoiceMan.LOGIC.VoiceManThread import VoiceManThread
from VoiceMan import __app_name__
from VoiceMan.GUI.Ribbon.RibbonWidget import RibbonWidget
from VoiceMan.LOGIC.VoiceOutThread import VoiceOutThread


class MainWindow(QMainWindow):
	stop_signal = pyqtSignal('PyQt_PyObject')

	def __init__(self, app):
		QMainWindow.__init__(self)
		self._ribbon = None
		self._ribbon_widget = None
		self._voice_man_thread = None
		self._voice_in_thread = None
		self._voice_out_thread = None
		self._record_thread = None
		self._input_scope = None
		self._output_scope = None
		self._freq_scope = None
		self._central_widget = None
		self._chunk_size = 256
		self._tone_harmonics = 32
		self.setMinimumWidth(1280*gui_scale())
		self.setMinimumHeight(800*gui_scale())
		self.setWindowTitle(__app_name__)

		self._audio = pyaudio.PyAudio()

		self._actions = {}
		self.init_actions()
		self.init_ribbon()
		self.init_home_tab()
		self.init_widgets()
		self.init_logic()

		self._voice_in_thread.new_chunk_signal.connect(self._input_scope.on_new_chunk)
		self._voice_man_thread.new_chunk_signal.connect(self._output_scope.on_new_chunk)
		self._voice_man_thread.new_freq_signal.connect(self._freq_scope.on_new_chunk)

	@property
	def voice_man(self):
		return self._voice_man_thread

	def init_widgets(self):
		self._input_scope = ScopeWidget(self, divisor=2, concatenate=6)
		self._freq_scope = ScopeWidget(self, chunk_type=np.float, max_magnitude=1000, offset=1.0)
		self._output_scope = ScopeWidget(self, divisor=2, concatenate=6)
		self._central_widget = QWidget(self)
		self._central_widget.setLayout(QVBoxLayout())
		scopes_widget = QWidget(self)
		scopes_widget.setLayout(QHBoxLayout())
		scopes_widget.layout().addWidget(self._input_scope)
		scopes_widget.layout().addWidget(self._freq_scope)
		scopes_widget.layout().addWidget(self._output_scope)
		self._central_widget.layout().addWidget(scopes_widget)
		controls_widget = QWidget(self)
		controls_widget.setLayout(QHBoxLayout())
		self._central_widget.layout().addWidget(controls_widget)

		pitch = QSlider(Qt.Vertical)
		pitch.setMaximum(200)
		pitch.setValue(100)
		pitch.valueChanged.connect(self.on_pitch_changed)

		controls_widget.layout().addWidget(pitch)

		for i in range(self._tone_harmonics):
			harmonic_control = HarmonicControl(self, i)
			controls_widget.layout().addWidget(harmonic_control)

		self.setCentralWidget(self._central_widget)

	def init_logic(self):
		self._voice_man_thread = VoiceManThread(chunk_size=self._chunk_size, tone_harmonics=self._tone_harmonics)
		self._voice_in_thread = VoiceInThread(self._audio, chunk_size=self._chunk_size)
		self._voice_out_thread = VoiceOutThread(self._audio, chunk_size=self._chunk_size)
		self._voice_in_thread.new_chunk_signal.connect(self._voice_man_thread.on_new_chunk)
		self._voice_man_thread.new_chunk_signal.connect(self._voice_out_thread.on_new_chunk)

		self.stop_signal.connect(self._voice_man_thread.on_stop)
		self.stop_signal.connect(self._voice_in_thread.on_stop)
		self.stop_signal.connect(self._voice_out_thread.on_stop)
		self._voice_man_thread.finished_signal.connect(self.on_finished)
		self._voice_in_thread.finished_signal.connect(self.on_finished)
		self._voice_out_thread.finished_signal.connect(self.on_finished)

	def init_actions(self):
		self._actions['open'] = self.add_action("Open\nPreset", "folder", "Open voice manipulator preset", True, self.on_open_preset, QKeySequence.Open)
		self._actions['save'] = self.add_action("Save\nPreset", "folder", "Save voice manipulator preset", True, self.on_save_preset, QKeySequence.Save)
		self._actions['record'] = self.add_action("Record\nAudio", "folder", "Record the audio", True, self.on_record, checkable=True)
		self._actions['start'] = self.add_action("Start\nVoiceMan", "start", "Start Voice manipulation", True, self.start_voice_man)
		self._actions['stop'] = self.add_action("Stop\nVoiceMan", "start", "Stop Voice manipulation", True, self.on_stop_click)

	def init_ribbon(self):
		self._ribbon = QToolBar(self)
		if is_dark_theme():
			self._ribbon.setStyleSheet(get_stylesheet("ribbon_dark"))
		else:
			self._ribbon.setStyleSheet(get_stylesheet("ribbon"))
		self._ribbon.setObjectName("ribbonWidget")
		self._ribbon.setWindowTitle("Ribbon")
		self.addToolBar(self._ribbon)
		self._ribbon.setMovable(False)
		self._ribbon_widget = RibbonWidget(self)
		self._ribbon_widget.currentChanged.connect(self.on_ribbon_changed)
		self._ribbon.addWidget(self._ribbon_widget)

	def init_home_tab(self):
		home_tab = self._ribbon_widget.add_ribbon_tab("Home")
		file_pane = home_tab.add_ribbon_pane("File")
		file_pane.add_ribbon_widget(RibbonButton(self, self._actions['open'], True))
		file_pane.add_ribbon_widget(RibbonButton(self, self._actions['save'], True))
		file_pane.add_ribbon_widget(RibbonButton(self, self._actions['record'], True))
		control_pane = home_tab.add_ribbon_pane("Control")
		control_pane.add_ribbon_widget(RibbonButton(self, self._actions['start'], True))
		control_pane.add_ribbon_widget(RibbonButton(self, self._actions['stop'], True))


	def add_action(self, caption, icon_name, status_tip, icon_visible, connection, shortcut=None, checkable=False) -> QAction:
		action = QAction(get_icon(icon_name), tr(caption, "ribbon"), self)
		action.setStatusTip(tr(status_tip, "ribbon"))
		action.triggered.connect(connection)
		action.setIconVisibleInMenu(icon_visible)
		if shortcut is not None:
			action.setShortcuts(shortcut)
		action.setCheckable(checkable)
		self.addAction(action)
		return action

	def start_voice_man(self):
		self._voice_man_thread.start()
		self._voice_in_thread.start()
		self._voice_out_thread.start()

	def on_finished(self):
		print("finished")

	def on_stop_click(self):
		self.stop_signal.emit(0)

	def on_ribbon_changed(self):
		pass

	def on_open_preset(self):
		pass

	def on_save_preset(self):
		pass

	def on_record(self, e):
		print(str(e))
		self._voice_out_thread.recording = e

	def on_pitch_changed(self, e):
		self._voice_man_thread.on_pitch_changed(e/100)

