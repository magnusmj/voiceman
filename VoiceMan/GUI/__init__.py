import pkgutil

from PyQt5.QtCore import QCoreApplication
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QApplication, QWidget

gui_scale_val = 0


def gui_scale():
	global gui_scale_val
	if gui_scale_val == 0:
		screen = QApplication.screens()[0]
		dpi = screen.logicalDotsPerInch()
		gui_scale_val = dpi / 96
	return gui_scale_val


def is_dark_theme():
	widget = QWidget()
	color = widget.palette().color(widget.backgroundRole())
	r = 0.0
	g = 0.0
	b = 0.0
	a = 0.0
	rgb = color.getRgb()
	if (rgb[0] + rgb[1] + rgb[2]) < 300:
		return True
	else:
		return False

stylesheet_instance = None

def get_stylesheet(name):
	global stylesheet_instance
	if not stylesheet_instance:
		stylesheet_instance = Stylesheets()
	return stylesheet_instance.stylesheet(name)

class Stylesheets(object):
	def __init__(self):
		self._stylesheets = {}
		self.make_stylesheet("main", "main.css")
		self.make_stylesheet("ribbon", "ribbon.css")
		self.make_stylesheet("ribbon_dark", "ribbon_dark.css")
		self.make_stylesheet("ribbonPane", "ribbonPane.css")
		self.make_stylesheet("ribbonButton", "ribbonButton.css")
		self.make_stylesheet("ribbonSmallButton", "ribbonSmallButton.css")

	def make_stylesheet(self, name, path):
		stylesheet = pkgutil.get_data('stylesheets', path).decode("utf8")

		self._stylesheets[name] = stylesheet

	def stylesheet(self, name):
		stylesheet = ""
		try:
			stylesheet = self._stylesheets[name]
		except KeyError:
			print("stylesheet " + name + " not found")
		return stylesheet

icons_instance = None


def get_icon(name):
	global icons_instance
	if not icons_instance:
		icons_instance = Icons()
	return icons_instance.icon(name)


class Icons(object):
	def __init__(self):
		self._icons = {}
		self.make_icon("default", "folder.png")
		app = QApplication.instance()
		#self._icons["default"] = app.style().standardIcon(QStyle.SP_DirOpenIcon)

	def make_icon(self, name, path):
		icon = QIcon()
		pixmap = QPixmap()
		data = pkgutil.get_data('icons', path)
		pixmap.loadFromData(data, "PNG")
		icon.addPixmap(pixmap, QIcon.Normal, QIcon.Off)
		self._icons[name] = icon

	def icon(self, name):
		icon = self._icons["default"]
		if type(name) is str:
			if name not in self._icons:
				file_name = name + ".png"
				try:
					self.make_icon(name, file_name)
				except IOError:
					print("file " + file_name + " not found")
		else:
			app = QApplication.instance()
			self._icons[name] = app.style().standardIcon(name)
		try:
			icon = self._icons[name]
		except KeyError:
			print("icon " + name + " not found")
		return icon

contexts = {}


def tr(string, context_name='app'):
	value = QCoreApplication.translate(context_name, string)
	try:
		context = contexts[context_name]
	except KeyError:
		contexts[context_name] = {}
		context = contexts[context_name]
	context[string] = value
	return value